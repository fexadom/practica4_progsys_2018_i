CC=gcc
CFLAGS=-c -I.
OBJECT=main.o validar.o confirmar.o

newuser: $(OBJECT)
	$(CC) -o newuser $(OBJECT) -I.

main.o: main.c 
	$(CC) $(CFLAGS) main.c

validar.o: validar.c validar.h
	$(CC) $(CFLAGS) validar.c

confirmar.o: confirmar.c confirmar.h
	$(CC) $(CFLAGS) confirmar.c

.PHONY: clean	
clean:
	rm -f *.o
	rm -f newuser

