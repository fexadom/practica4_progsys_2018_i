#include <validar.h>

int validar(Usuario_t *u){
	
	u->nombre[0] = toupper(u->nombre[0]);
	u->apellido[0] = toupper(u->apellido[0]);
	
    int error = 0;
    
    if(strlen(u->username) < 8){
		error = 1;
	}
	else if(strlen(u->username) > 10){
		error = 2;
	}
	else if(strlen(u->password) < 10){
		error = 3;
	}
	else if(hasChars(u->password)==0){
		error = 4;
	}
	else if(hasNumbers(u->password)==0){
		error = 5;
	}
	
	return error;
}

Usuario_t getUser(){
	
    Usuario_t newUser;
    
    printf("Ingrese nombre: \n");
    scanf("%s", newUser.nombre);
    
    printf("Ingrese apellido: \n");
    scanf("%s", newUser.apellido);
    
    printf("Ingrese username: \n");
    scanf("%s", newUser.username);
    
    printf("Ingrese password: \n");
    scanf("%s", newUser.password);
    
    newUser.userid = rand();
    return newUser;
}

int hasChars(char string[]){
	int chars = 0;
	for(int i = 0; i < strlen(string); i++){
		if(isalpha(string[i])){
			chars++;
		}
	}
	return chars;
}


int hasNumbers(char string[]){
	int digits = 0;
	for(int i = 0; i < strlen(string); i++){
		if(isdigit(string[i])){
			digits++;
		}
	}
	return digits;
}


//FUNCIONALIDAD AGREGADA EN PRACTICA "COMPILANDO CON MAKE"
void getUserName(Usuario_t *u){
	do{
		if(strlen(u->username) < 8){
			fprintf(stderr, "Error! Username < 8 \n");
		}
		else if(strlen(u->username) > 10){
			fprintf(stderr, "Error! Username > 10 \n");
		}
		printf("Ingrese username: \n");
		scanf("%s", u->username);
	}
	while(strlen(u->username) < 8 || strlen(u->username) > 10);
}

void getPassword(Usuario_t *u){
	do{
		if(strlen(u->password) < 10){
			fprintf(stderr, "Error! Password < 10 \n");
		}
		else if(hasChars(u->password)==0){
			fprintf(stderr, "Error! Password NO contiene letras\n");
		}
		else if(hasNumbers(u->password)==0){
			fprintf(stderr, "Error! Password NO contiene digitos\n");
		}
		printf("Ingrese password: \n");
		scanf("%s", u->password);
	}
	while(strlen(u->password) < 10 || hasChars(u->password)==0 || hasNumbers(u->password)==0);
}
