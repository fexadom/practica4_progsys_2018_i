#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define MAXSTR 50
#define MSG "Usuario Registrado Exitosamente!"

typedef struct Usuario{
 char nombre[MAXSTR];
 char apellido[MAXSTR];
 char username[MAXSTR];
 char password[MAXSTR];
 int userid;
}Usuario_t;


int validar(Usuario_t *);
Usuario_t getUser(void);
int hasChars(char[]);
int hasNumbers(char[]);

//FUNCIONALIDAD AGREGADA EN PRACTICA "COMPILANDO CON MAKE"
void getUserName(Usuario_t *);
void getPassword(Usuario_t *);
